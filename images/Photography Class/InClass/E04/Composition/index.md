The first images shows how the balanced/asymmetric images place emphasis.

The 3rd image is an example of a general-composition. It looks like an easy shot, but in fact it was very hard to extract emphasis from the image, and create a generic-balanced look.

The last one shows how the rule of thirds places the main object in an interesting position. The intersection of the door-wall-bottle line makes the view comfortable on the eye.
